jQuery.tinydot
================

Tiny jQuery plugin for cross-browser ellipsis on multiple line content.<br />
Demo's and documentation: http://tinydot.modos189.ru

*Note:*<br />
Because its performance can not be improved, this plugin is no longer actively maintained.<br />
Feel free to use it and submit pull requests though.


## How to use the plugin
### Integration to your page

Include all necessary .js-files inside the head-tag of the page.

```html
<head>
    <script src="jquery.js" type="text/javascript"></script>
    <script src="jquery.tinydot.js" type="text/javascript"></script>
</head>
```

Then you can use either CSS or JS approach or use them both.

### CSS approach
You can add one or several CSS classes to HTML elements to automatically invoke "jQuery.tinydot functionality" and some extra features. It allows to use jQuery.tinydot only by adding appropriate CSS classes without JS programming.

Available classes and their description:
* dot-ellipsis - automatically invoke jQuery.tinydot to this element. This class must be included if you plan to use other classes below.
* dot-resize-update - automatically update if window resize event occurs. It's equivalent to option `watch:'window'`.

*Examples*

Adding jQuery.tinydot to element:

```html
<div class="dot-ellipsis">
    <p>Lorem Ipsum is simply dummy text.</p>
</div>
```

Adding jQuery.tinydot to element with update on window resize:
    
```html
<div class="dot-ellipsis dot-resize-update">
    <p>Lorem Ipsum is simply dummy text.</p>
</div>
```


## Javascript approach
Create a DOM element and put some text and other HTML markup in this "wrapper".

```html
<div id="wrapper">
    <p>Lorem Ipsum is simply dummy text.</p>
</div>
```

Fire the plugin onDocumentReady using the wrapper-selector.

```javascript
$(document).ready(function() {
    $("#wrapper").tinydot({
        // configuration goes here
    });
});
```

### Authors and Contributors
* [Alexander Danilov](https://gitlab.com/u/modos189) is the author of the jQuery.tinydot.
* [Fred Heusschen](https://github.com/FrDH) is the author of the jQuery.dotdotdot, by analogy with which was created by this plugin.

### More info
Please visit http://tinydot.modos189.ru
Repository on Gitlab: https://gitlab.com/modos189/jQuery.tinydot

### Licence
The jQuery.tinydot plugin is licensed under the MIT license:
http://en.wikipedia.org/wiki/MIT_License
